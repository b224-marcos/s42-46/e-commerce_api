const Product = require("../models/Product"); 
const User = require("../models/User"); 
const auth = require("../auth"); 




// Function for Admin only Create Product
module.exports.createNewProduct = (reqBody, userData) => {
											
 	return User.findById(userData.userId).then(result => {

		if (userData.isAdmin === false) { 

			return false

		} else { 
			
			let newProduct = new Product ({

				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price

			});

			return newProduct.save().then((product, error) => {
				if (error) { 
					return false

				} else { 

					return true

				}
			})
		}
	})
};


// Function for Retrieving Active products
module.exports.showProducts = () => {

	return Product.find({isActive: true}).then(result => {

		return result;

	});

};

module.exports.getAllProducts = (data) => {
	
	return Product.find({}).then((result, error) => {
			if(error) {
				return false
			} else {
				return result
			}
		})
	};





// Function for Retrieving Single Product

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	});

};

// Function for Updating Product Information
module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error) => {
		console.log(result)
		if(error) {
			return false
		} else {
			return true
		}
	})
};


// Function for Archiving a Product
 module.exports.archiveProduct = (reqParams, reqBody) => {
    
    let archiveProduct = {
        isActive: reqBody.isActive
 }
    return Product.findByIdAndUpdate(reqParams.productId, archiveProduct).then((archiveProduct, error) => {
               
       if(error) {
           return false
      } else {
            return true
           }
      })
};

module.exports.activateProduct = (reqParams, reqBody) => {
    
    let activateProduct = {
        isActive: reqBody.isActive
 }
    return Product.findByIdAndUpdate(reqParams.productId, activateProduct).then((activateProduct, error) => {
               
       if(error) {
           return false
      } else {
            return true
           }
      })
};


///////////////////////////////////////////////////////////////////////////////////////////////////////////


// Function for Retrieving Products by an Admin 

/*module.exports.getAllProducts = (data) => {
	
	if(data.isAdmin) {
		return Product.find({}).then((result, error) => {

			if(error) {
				return false
			} else {
				return result
			}
		})
	}
}*/









