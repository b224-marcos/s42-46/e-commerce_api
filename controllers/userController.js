const User = require("../models/User"); 
const Product = require("../models/Product");
const auth = require("../auth"); 
const bcrypt = require("bcrypt"); // for hashing password





// Function for checking Duplicate Email

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		//console.log(result.email)
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
};


// Function for User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
			
	});

	return newUser.save().then((user, error) => {
		if (error) { 
			return false
		} else { 
			return true
		}
	});
};


// Function for Logging in and Authentication

module.exports.loginUser = (reqBody) => {			
	return User.findOne({email: reqBody.email}).then(result => {
		console.log(result)

		if(result === null) {
			return false
		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {

				return { access: auth.createAccessToken(result)}

			} else {
				return false
			}
		}
	})
};


// Function for retrieving User Details

module.exports.getUserDetails = (userData) => {
	return User.findById(userData.id).then(result => {
				
		if (result == null) {
			return false
		} else {
			result.password = "**"
			return result
		}
	})
};


/*// Function for Creating a Product
module.exports.orderProduct = async (data) => {

	if (data.isAdmin == true) {
		return false
	} else {
		let isUserUpdated = await User.findById(data.userId).then(user => {

			user.orderedProduct.push({productId: data.productId, productName: data.productName, quantity: data.quantity});
			return user.save().then((user, error) => {
				if(error) {
					return false
				} else {
					return true
				}
			})
		});

		let isProductUpdated = await Product.findById(data.productId).then(product => {
			console.log(data.userId)
			product.userOrder.push({userId: data.userId});

			return product.save().then((product, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})
		});

		if (isUserUpdated && isProductUpdated) {
			return true
		} else {
			return false
		};
	}
};
*/

// Function for Creating a Product
module.exports.orderProduct = async (data) => {
	console.log(data)
	if (data.isAdmin == true) {
		return false
	} else {
		let isUserUpdated = await User.findById(data.userId).then(user => {

			user.orderedProduct.push({
				products: {
				productId: data.productId, 
				productName: data.productName, 
				quantity: data.quantity
			}
		});

			return user.save().then((user, error) => {
				if(error) {
					return false
				} else {
					return true
				}
			})
		});

		let isProductUpdated = await Product.findById(data.productId).then(product => {
			console.log(data.userId)
			product.userOrder.push({userId: data.userId});

			return product.save().then((product, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})
		});

		if (isUserUpdated && isProductUpdated) {
			return true
		} else {
			return false
		};
	}
};







	





