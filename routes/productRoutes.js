const express = require("express");
const router = express.Router();
const productController = require ("../controllers/productController");
const auth = require ("../auth");





// Creating Product by an Admin Route

router.post("/addProduct", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	productController.createNewProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController));

});


// Retrieving All Active Products Route

router.get("/allProducts", (req, res) => {

	productController.showProducts().then(resultFromController => res.send(resultFromController));
});


// Retrieving products by an admin
router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin) {
		productController.getAllProducts(req.params, req.bod).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	}
});




// Retrieving Single Product Route
router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

});



// Updating Product Information by an Admin Route
router.put("/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth:"failed"})
	}
});


// Archiving a Product Route

router.put("/archive/:productId", auth.verify, (req, res) => {

     const userData = auth.decode(req.headers.authorization)

     if(userData.isAdmin) {
         productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
     } else {
         res.send({auth: "failed"})
     }

 });

//

router.put("/activate/:productId", auth.verify, (req, res) => {

     const userData = auth.decode(req.headers.authorization)

     if(userData.isAdmin) {
         productController.activateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
     } else {
         res.send({auth: "failed"})
     }

 });


//////////////////////////////////////////////////////////////////////

/*// Retrieving Products by an admin

router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController))
});*/













module.exports = router;
