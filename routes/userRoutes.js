const express = require("express");
const router = express.Router();
const userController = require ("../controllers/userController");
const auth = require("../auth");



// Check for duplicate email Route

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController)) 
});

// User Registration Route

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// User LogIn & Authentication Route
router.post("/login", (req, res) => {

		userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));

	});


// Retrieving User details Route
router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	userController.getUserDetails({id: userData.id}).then(resultFromController => res.send(resultFromController))
});




// Route for Creating an Order

router.post("/orderProduct", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization)

	let data = {
		isAdmin: userData.isAdmin,
		userId: userData.id,		
		productId: req.body.productId,
		productName: req.body.productName,
		quantity: req.body.quantity
	}
	
	userController.orderProduct(data).then(resultFromController => {
		res.send(resultFromController)
	})
});





module.exports = router