const express = require('express');
const mongoose = require('mongoose');
const cors = require("cors");
const userRoutes = require('./routes/userRoutes')
const productRoutes = require("./routes/productRoutes")
mongoose.set('strictQuery', true);

const app = express();
const port = process.env.PORT || 8000;


// MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({extended: true}));


//MAIN URI
app.use("/users", userRoutes);
app.use("/products", productRoutes)



// MONGOOSE CONNECTION
mongoose.connect("mongodb+srv://napmarcos:admin123@marcos.hsczop7.mongodb.net/CAPSTONE_API?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}

);


let db = mongoose.connection;

db.on("error", () => console.error("Connection error"));
db.once("open", () => console.log("Connected to MongoDB"))



app.listen(port, () => {console.log(`API is now running at port ${port}`)});