const mongoose = require("mongoose");



const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},

	lastName: {
		type: String,
		required: [true, "First Name is required"]
	},

	mobileNo: {
		type: String,
		required: [true, "Mobile Number required"]
	},

	email: {
		type: String,
		required: [true, "Email Adress required"]
	},

	password: {
		type: String,
		required: [true, "Password required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	orderedProduct: [{

		products: [{

			productId: {
				type: String,
				required: [true, "Product ID required"]
			},


			productName: {
				type: String,
				required: [true, "Product Name is required"]
			},

			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			}
		}],

		totalAmount: {
			type: Number,
			
		},

		purchasedOn: {
			type: Date,
			default: new Date()
		}

	}]


});

	


module.exports = mongoose.model("User", userSchema)